﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextPoint;

namespace SoMa_NightMode
{
    public class NightMode : IExtension
    {
        private ITextPoint HostApplication;

        public void Initialize(ITextPoint hostApplication)
        {
            HostApplication = hostApplication;
        }

        public string GetTitle()
        {
            return "NightMode";
        }

        public void Execute()
        {
            HostApplication.SetBackgroundColor(Color.FromArgb(65, 65, 65));
            HostApplication.SetForegroundColor(Color.WhiteSmoke);
        }
    }
}
