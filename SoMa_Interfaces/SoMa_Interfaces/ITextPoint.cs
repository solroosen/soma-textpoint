﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TextPoint
{
    public interface ITextPoint
    {
        void SetBackgroundColor(Color col);
        void SetForegroundColor(Color col);
        void SetFont(Font font);
        string Gettext();
        void SetText(string text);
    }
}
